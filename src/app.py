import streamlit as st
import boto3
import pandas as pd
from io import StringIO

# AWS S3 settings
BUCKET_NAME = 'sampledata'

ENDPOINT_URL = 'http://localhost:9000'
ACCESS_KEY = 'your_access_key'
SECRET_KEY = 'your_secret_key'
BUCKET_NAME = 'sampledata'

# Configure the S3 client
s3 = boto3.client(
    's3',
    endpoint_url=ENDPOINT_URL,
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    # Specify your region if needed
    # For MinIO, you may need to disable SSL verification
    config=boto3.session.Config(signature_version='s3v4'),
    verify=False
)


# Streamlit UI
st.title('S3 Data Query App')

# User input for tracking ID
tracking_id = st.text_input('Enter the tracking ID:')

# Button to trigger the query
if st.button('Query'):
    try:
        # List all objects in the bucket
        objects = s3.list_objects_v2(Bucket=BUCKET_NAME)
        
        # Initialize an empty list to store data frames
        dfs = []
        
        # Iterate over each object in the bucket
        for obj in objects.get('Contents', []):
            key = obj['Key']
            
            # Skip non-JSON files
            if not key.endswith('.json'):
                continue
            
            # Download the object from S3
            response = s3.get_object(Bucket=BUCKET_NAME, Key=key)
            data = response['Body'].read().decode('utf-8')
            
            # Convert the JSON data to a DataFrame
            df = pd.read_json(StringIO(data), orient='records')
            filtered_df = df[df['trackingnumber'] == int(tracking_id)]
            dfs.append(filtered_df)
        
        # Concatenate all data frames into a single DataFrame
        combined_df = pd.concat(dfs, ignore_index=True)
        
        # Filter the combined DataFrame based on the tracking ID

        
        # Display the filtered data
        st.write(combined_df)
        
    except Exception as e:
        st.error(f"An error occurred: {e}")
