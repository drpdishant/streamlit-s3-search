import json
import pymongo
import boto3
from os import getenv

# MongoDB connection settings
MONGO_URI = getenv("MONGO_URI")
DB_NAME = 'test'
COLLECTION_NAME = 'sampleData'

ENDPOINT_URL = 'http://localhost:9000'
ACCESS_KEY = 'your_access_key'
SECRET_KEY = 'your_secret_key'
BUCKET_NAME = 'sampledata'

# Configure the S3 client
s3 = boto3.client(
    's3',
    endpoint_url=ENDPOINT_URL,
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    # Specify your region if needed
    # For MinIO, you may need to disable SSL verification
    config=boto3.session.Config(signature_version='s3v4'),
    verify=False
)


# Connect to MongoDB
client = pymongo.MongoClient(MONGO_URI)
db = client[DB_NAME]
collection = db[COLLECTION_NAME]

# Batch size
batch_size = 100

# Query and upload collection data in batches
cursor = collection.find()
total_documents = collection.count_documents({})  # Get total number of documents

batch_num = 0
for i, document in enumerate(cursor):
    if i % batch_size == 0:
        batch = []
    batch.append(document)

    if len(batch) == batch_size or i == total_documents - 1:
        # Convert batch to JSON
        data = json.dumps(batch, default=str)

        # Upload batch to S3
        object_key = f'batch_{batch_num}.json'
        s3.put_object(Bucket=BUCKET_NAME, Key=object_key, Body=data)

        batch_num += 1

print("Collection archived to S3 successfully.")