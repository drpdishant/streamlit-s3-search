import pandas as pd
import random
from datetime import datetime, timedelta
from pymongo.mongo_client import MongoClient
from os import getenv

# Generate 1000 sample documents
documents = []
start_date = datetime.now() - timedelta(days=90)  # 3 months ago
for _ in range(1000):
    tracking_number = random.randint(100000, 999999)
    creation_timestamp = start_date + timedelta(days=random.randint(0, 90))
    update_timestamp = creation_timestamp + timedelta(days=random.randint(0, 90))
    documents.append({
        'trackingnumber': tracking_number,
        'creation_timestamp': creation_timestamp,
        'update_timestamp': update_timestamp
    })

# Create a DataFrame from the documents list
df = pd.DataFrame(documents)

# Convert timestamps to string format
df['creation_timestamp'] = df['creation_timestamp'].dt.strftime('%Y-%m-%d %H:%M:%S')
df['update_timestamp'] = df['update_timestamp'].dt.strftime('%Y-%m-%d %H:%M:%S')

# Print the first few documents
print(df.head())

uri = getenv("MONGO_URI")

client = MongoClient(uri)
db = client['test']
collection = db['sampleData']

# Insert documents into MongoDB
records = df.to_dict(orient='records')
collection.insert_many(records)

print("Documents inserted successfully.")
